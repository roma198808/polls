class Response < ActiveRecord::Base
  attr_accessible :user_id, :question_id, :answer_choice_id

  validate :respondent_has_not_already_answered_question
  validate :respondent_is_not_author


  validates :user_id, :presence => true
  validates :question_id, :presence => true
  validates :answer_choice_id, :presence => true

  belongs_to(
  :answer_choice,
  :foreign_key => :answer_choice_id
  )

  belongs_to(
  :respondent,
  :class_name => "User",
  :foreign_key => :user_id
  )


  private

  def respondent_has_not_already_answered_question
    unless existing_responses.empty?
      errors[:duplicate] << "You already answered that question!"
    end
  end

  def existing_responses
    Response.find_by_sql [
    "SELECT * FROM responses
    WHERE user_id = ?
    AND question_id = ?",
    self.user_id, self.question_id]
  end

  def respondent_is_not_author
    if users_questions.include?(self.question_id)
      errors[:author] << "You wrote that question!"
    end
  end

  def users_questions
    polls = User.find(self.user_id).authored_polls

    questions = []
    polls.each do |poll|
      questions += poll.questions
    end
    questions.map { |question| question.id }
  end

end