class AnswerChoice < ActiveRecord::Base
  attr_accessible :text, :question_id
  validates :text, :presence => true
  validates :question_id, :presence => true

  belongs_to(
  :question,
  :foreign_key => :question_id
  )

  has_many(
  :responses,
  :foreign_key => :answer_choice_id
  )
end