class Question < ActiveRecord::Base
  attr_accessible :text, :poll_id
  validates :text, :presence => true
  validates :poll_id, :presence => true

  has_many(
  :answer_choices,
  :foreign_key => :question_id
  )

  belongs_to(
  :poll,
  :foreign_key => :poll_id
  )
end