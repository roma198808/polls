class Poll < ActiveRecord::Base
  attr_accessible :title, :author_id
  validates :title, :presence => true

  belongs_to(:author, :class_name => "User")

  has_many(
  :questions,
  :foreign_key => :poll_id
  )
end